package it.umsida.hanifahdwindasari.beautyask.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 25/04/2016.
 */
public class DatabaseAccessMasalah {
    private static final String TABLE_NAME = "Masalah_Wajah";
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccessMasalah instance;

    private DatabaseAccessMasalah(Context context){
        this.openHelper = new DatabaseOpenHelper(context);
    }
    public static DatabaseAccessMasalah getInstance(Context context){
        if (instance == null){
            instance = new DatabaseAccessMasalah(context);
        }
        return instance;
    }
    public void open(){
        this.database = openHelper.getWritableDatabase();
    }
    public  void close(){
        if (database != null){
            this.database.close();
        }
    }
    public List<String> getListMasalah(){
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT nama_masalah FROM "+TABLE_NAME, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public HashMap getDetailMasalahWajah(String masalah){
        HashMap detail = new HashMap();
        Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE nama_masalah="+"'"+masalah+"'", null);
        cursor.moveToFirst();
        detail.put("id",cursor.getString(0));
        detail.put("nama",cursor.getString(1));
        detail.put("deskripsi",cursor.getString(2));
        detail.put("gambar",cursor.getString(3));
        cursor.close();
        return detail;
    }
}