package it.umsida.hanifahdwindasari.beautyask.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by user on 25/04/2016.
 */
public class DatabaseOpenHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "informasi_jenis_facial.sqlite";
    private static final int DATABASE_VERSION = 1;

    public DatabaseOpenHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, oldVersion, newVersion);
        db.execSQL("DROP TABLE IF EXISTS Facial");
        db.execSQL("DROP TABLE IF EXISTS Gejala");
        db.execSQL("DROP TABLE IF EXISTS Jenis_Facial");
        db.execSQL("DROP TABLE IF EXISTS Masalah_Wajah");
        db.execSQL("DROP TABLE IF EXISTS Produk_Harian");
        db.execSQL("DROP TABLE IF EXISTS rekap_data");
        db.execSQL("DROP TABLE IF EXISTS Solusi_Umum");
        db.execSQL("DROP TABLE IF EXISTS Transaksi");
        db.execSQL("DROP TABLE IF EXISTS Tips_Kecantikan");
        onCreate(db);
    }
}
