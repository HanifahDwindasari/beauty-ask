package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessFacial;

public class NavDrawerFacialWajah extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView facial_wajah;
    DatabaseAccessFacial databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer_facial_wajah);
        facial_wajah = (ListView) findViewById(R.id.list_facial_wajah);
        databaseAccess = DatabaseAccessFacial.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        databaseAccess.open();

        //search
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.nav_drawer_facial_wajah, menu){
//            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//            SearchView searchView = (SearchView) menu.findItem(R.id.pencarian2).getActionView();



        final List<String> facialwajah = databaseAccess.getListFacial();
        String[] fw = facialwajah.toArray(new String[facialwajah.size()]);
        int[] imgid = {
                R.drawable.facial1,
                R.drawable.facial2,
                R.drawable.facial3,
                R.drawable.facial4,
                R.drawable.facial5,
                R.drawable.facial6,
                R.drawable.facial7,
                R.drawable.facial8,
                R.drawable.facial9,
                R.drawable.facial10,
                R.drawable.facial11,
                R.drawable.facial12,
                R.drawable.facial13,
                R.drawable.facial14,
                R.drawable.facial15,
                R.drawable.facial16,
                R.drawable.facial17,
                R.drawable.facial18,
                R.drawable.facial19,
                R.drawable.facial20,
                R.drawable.facial21,
                R.drawable.facial22,
                R.drawable.facial23,
                R.drawable.facial24,
                R.drawable.facial25,
                R.drawable.facial26,
                R.drawable.facial27,
                R.drawable.facial28,
                R.drawable.facial29,

        };

        CustomAdapterFacialWajah adapter = new CustomAdapterFacialWajah(this, fw, imgid);
        facial_wajah.setAdapter(adapter);

        facial_wajah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                HashMap detail = databaseAccess.getDetailFacial(facialwajah.get(position));
//                Toast.makeText(NavDrawerFacialWajah.this, facialwajah.get(position) , Toast.LENGTH_SHORT).show();
                Intent i = new Intent(NavDrawerFacialWajah.this,DetailFacialWajahs.class);
                i.putExtra("map",detail);
                startActivity(i);
                //Toast.makeText(InformasiMasalahWajah.this, "Nama : "+detail.get("nama"), Toast.LENGTH_SHORT).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.nav_drawer_facial_wajah, menu);
//        MenuItem item = menu.findItem(R.id.pencarian2);
//        SearchView searchView =  (SearchView) MenuItemCompat.getActionView(item);
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//        });
//        return true;
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.nav_drawer_facial_wajah, menu);
//        SearchManager searchManager =  (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.pencarian2).getActionView();
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//
//        return true;
//    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_1) {
            HashMap detail = databaseAccess.getDetailFacial("Totok Aura Wajah");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_2) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Standart");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_3) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Fruitty");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_4) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Tea Tree");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_5) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Whitening");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_6) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Collagen");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_7) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Detox");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_8) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Anti Aging");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_9) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Glowing Gold");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_10) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Acne Premium");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_11) {
            HashMap detail = databaseAccess.getDetailFacial("Facial H&M Lightening");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_12) {
            HashMap detail = databaseAccess.getDetailFacial("Facial H&M Acne");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_13) {
            HashMap detail = databaseAccess.getDetailFacial("Facial H&M Sensitive");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_14) {
            HashMap detail = databaseAccess.getDetailFacial("Facial H&M Oily Skin");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_15) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Sensitive");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_16) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Diamond Premium");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_17) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Glowing Gold Premium");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_18) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Anti Aging Premium");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_19) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Radical White");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_20) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Mesotherapy Whitening");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_21) {
            HashMap detail = databaseAccess.getDetailFacial("Facial Mesotherapy Acne");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_22) {
            HashMap detail = databaseAccess.getDetailFacial("Chemical Peeling");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_23) {
            HashMap detail = databaseAccess.getDetailFacial("Derma Peel H&M");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_24) {
            HashMap detail = databaseAccess.getDetailFacial("Mikrodermabrasi");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_25) {
            HashMap detail = databaseAccess.getDetailFacial("Cauter");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_26) {
            HashMap detail = databaseAccess.getDetailFacial("RF Lifting Wajah");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_27) {
            HashMap detail = databaseAccess.getDetailFacial("Derma Roller");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_28) {
            HashMap detail = databaseAccess.getDetailFacial("Derma Filler");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_29) {
            HashMap detail = databaseAccess.getDetailFacial("Botox");
            Intent i = new Intent(NavDrawerFacialWajah.this, DetailFacialWajahs.class);
            i.putExtra("map", detail);
            startActivity(i);
        }
//

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}

