package it.umsida.hanifahdwindasari.beautyask;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by user on 12/05/2016.
 */
public class CustomAdapterMasalahWajah extends ArrayAdapter<String> {

        private final Activity context;
        private final String[] nama_masalah;
        private final int[] imgid;

        public CustomAdapterMasalahWajah(Activity context, String[] nama_masalah, int[] imgid) {
            super(context, R.layout.list_info_masalah_wajah, nama_masalah);
            // TODO Auto-generated constructor stub

            this.context=context;
            this.nama_masalah=nama_masalah;
            this.imgid=imgid;
        }

        public View getView(int position,View view,ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.list_info_masalah_wajah, null,true);

            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView1);
            TextView tv_nama_masalah = (TextView) rowView.findViewById(R.id.textView1);
            TextView tv_sekilas_masalah_wajah = (TextView) rowView.findViewById(R.id.textView2);
            TextView tv_lihat_penjelasan = (TextView) rowView.findViewById(R.id.textView3);

            tv_nama_masalah.setText(nama_masalah[position]);
            tv_sekilas_masalah_wajah.setText("Sekilas mengenai "+ nama_masalah[position]);
            tv_lihat_penjelasan.setText("LIHAT PENJELASAN");
            imageView.setImageResource(imgid[position]);
            return rowView;

        };
}
