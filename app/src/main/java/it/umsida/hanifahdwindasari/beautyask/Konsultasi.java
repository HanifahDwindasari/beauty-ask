package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by user on 16/05/2016.
 */
public class Konsultasi extends AppCompatActivity {
    ImageView tombolmulai;

    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.halaman_awal_konsultasi);
        super.onCreate(savedInstanceState);
        ImageButton tombolmulai = (ImageButton) findViewById(R.id.mulai);
        tombolmulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pindah = new Intent(Konsultasi.this, KonsultasiMasalahWajah.class);
                startActivity(pindah);
                finish();
            }
        });
    }
}
