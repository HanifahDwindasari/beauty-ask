package it.umsida.hanifahdwindasari.beautyask;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;

public class DetailMasalahWajahActivity extends AppCompatActivity {

    ScaleGestureDetector scaleGestureDetector;
    TextView txt_detail, judulmasalahwajah;
    ImageView gambar1;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_masalah_wajah);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //deskripsi
        txt_detail = (TextView) findViewById(R.id.txt_detail);
        Intent i = getIntent();
        HashMap data = (HashMap) i.getSerializableExtra("map");
        setTitle((String) data.get("nama"));
//        int gambar = Integer.parseInt((String)data.get("gambar"));
        txt_detail.setText(Html.fromHtml(data.get("deskripsi").toString()));
        //Scale
        scaleGestureDetector = new ScaleGestureDetector(this, new simpleOnScaleGestureListener());
        //
        gambar1 = (ImageView) findViewById(R.id.gambar_1);
        judulmasalahwajah = (TextView) findViewById(R.id.text_judul_detail_masalah_wajah);

        judulmasalahwajah.setText(data.get("nama").toString());

        try {
           /* InputStream inputStream = getResources().getDrawable().open("jerawat.png");
            Drawable d = Drawable.createFromResourceStream(inputStream,null);
            Bitmap bp = BitmapFactory.decodeStream();
            gambar_masalah.setImageBitmap(bmp);
            */
          Context con = gambar1.getContext();
//            int id = getResources().getIdentifier(String.valueOf(data.get("gambar_masalah")),null,con.getPackageName());
//            gambar1.setImageResource(id);

//            gambar1.setImageResource(R.drawable.jerawat);
            switch ((String) data.get("nama")){
                case "Jerawat" :
                    gambar1.setImageResource(R.drawable.jerawat);
                    break;
                case"Flek" :
                    gambar1.setImageResource(R.drawable.flek);
                    break;
                case "Berminyak" :
                    gambar1.setImageResource(R.drawable.berminyak);
                    break;
                case "Kerutan" :
                    gambar1.setImageResource(R.drawable.kerutan);
                    break;
                case "Kusam" :
                    gambar1.setImageResource(R.drawable.kusam);
                    break;
                case "Kering" :
                    gambar1.setImageResource(R.drawable.kering);
                    break;
                default:
                    break;
            }
        } catch (Exception e){
            return;
        }


        //foto
        // String strPath = data.get("gambar").toString();
        /*
        ImageView imgPhoto = (ImageView) findViewById(R.id.gambar_masalah);
        imgPhoto.setPadding(5, 5, 5, 5);
        imgPhoto.setImageResource(R.drawable.jerawat);
        //imgPhoto.setImageBitmap();
        Bitmap bm = BitmapFactory.decodeFile(strPath);

        int width=100;
        int height=100;
        Bitmap resizedbitmap = Bitmap.createScaledBitmap(bm, width, height, true);
        imgPhoto.setImageBitmap(resizedbitmap);


        try {
            InputStream is = getAssets().open("jerawat.png");
            Drawable d = Drawable.createFromStream(is,null);

            ImageView im = (ImageView)findViewById(R.id.gambar_masalah);


        } catch () {
            return;
        }*/

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailMasalahWajah Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailMasalahWajah Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
    //scale
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    public class simpleOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            float size = txt_detail.getTextSize();
            Log.d("TextSizeStart", String.valueOf(size));

            float factor = detector.getScaleFactor();
            Log.d("Factor", String.valueOf(factor));

            float product = size * factor;
            Log.d("TextSize", String.valueOf(product));
            txt_detail.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);

            size = txt_detail.getTextSize();
            Log.d("TextSizeEnd", String.valueOf(size));
            return true;
        }
    }
}

