package it.umsida.hanifahdwindasari.beautyask;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by user on 25/05/2016.
 */
public class CustomAdapterFacialWajah extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] nama_facial;
    private final int[] imgid;

    public CustomAdapterFacialWajah(Activity context, String[] nama_facial, int[] imgid) {
        super(context, R.layout.list_info_facial_wajah, nama_facial);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.nama_facial = nama_facial;
        this.imgid=imgid;
    }

    public View getView(int position, View view, ViewGroup parent) {
        View rowView = view;
        if( rowView == null ) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.list_info_facial_wajah, null, true);
        }
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView2);
        TextView tv_nama_facial = (TextView) rowView.findViewById(R.id.textView4);
        TextView tv_sekilas_facial_wajah = (TextView) rowView.findViewById(R.id.textView5);
        TextView tv_lihat_penjelasan = (TextView) rowView.findViewById(R.id.textView6);

        if( position < imgid.length ) {
            tv_nama_facial.setText(nama_facial[position]);
            tv_sekilas_facial_wajah.setText("Sekilas mengenai " + nama_facial[position]);
            tv_lihat_penjelasan.setText("LIHAT PENJELASAN");
            imageView.setImageResource(imgid[position]);
        }
        return rowView;
    };
}
