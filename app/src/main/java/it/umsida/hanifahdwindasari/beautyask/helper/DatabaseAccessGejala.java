package it.umsida.hanifahdwindasari.beautyask.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 25/05/2016.
 */
public class DatabaseAccessGejala {
    private static final String TABLE_NAME = "Gejala";
    private static final String TABLE_TRANSAKSI = "Transaksi";
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private Context context;
    private static DatabaseAccessGejala instance;

    public DatabaseAccessGejala(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
        this.context = context;
    }

    public static DatabaseAccessGejala getInstance(Context context) {
        if(instance == null)
            instance = new DatabaseAccessGejala(context);

        return instance;
    }

    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    public void close() {
        if (database != null)
            this.database.close();
    }

    public HashMap getGejala(String id_gejala) {
        HashMap hashMap = new HashMap();
        Cursor cursor;
        if(null == id_gejala)
            cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY id_gejala ASC", null);
        else cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id_gejala = '" + id_gejala + "'", null);
        cursor.moveToFirst();

        hashMap.put("id_gejala", cursor.getString(0));
        hashMap.put("nama_gejala", cursor.getString(1));
        hashMap.put("deskripsi", cursor.getString(2));
        cursor.close();
        return hashMap;
    }

    public HashMap getGejalaBerikutnya(String id, String jawaban) {
        HashMap hashMap = new HashMap();
        Cursor cursorTransaksi;
        if(jawaban.equals("y")) {
            cursorTransaksi = database.rawQuery("SELECT y AS jawaban,id  FROM " + TABLE_TRANSAKSI + " WHERE id = '" + id + "'", null);
            System.out.println("Pilihan ya");
        }else {
            cursorTransaksi = database.rawQuery("SELECT t AS jawaban,id FROM " + TABLE_TRANSAKSI + " WHERE id = '" + id + "'", null);
            System.out.println("Pilihan tidak");
        }
        cursorTransaksi.moveToFirst();
        String berikutnya = cursorTransaksi.getString(0);
        System.out.println("Berikutnya " + id);
        if(cursorTransaksi.getString(0).matches("S.*")) {
            hashMap.put("status", "solusi");
            hashMap.put("solusi", cursorTransaksi.getString(0));
        }else {
            hashMap.put("status", "gejala");
            hashMap.put("id", cursorTransaksi.getString(0));
            Cursor cursor = database.rawQuery("SELECT gejala FROM " + TABLE_TRANSAKSI + " where id = '" +
                                        cursorTransaksi.getString(0) + "'", null);
            cursor.moveToFirst();
            hashMap.putAll(getGejala(cursor.getString(0)));
            cursor.close();
        }
        cursorTransaksi.close();
        return hashMap;
    }
    public boolean deleteTransaksi(String id){
        Cursor cursor = database.rawQuery("SELECT id FROM " + TABLE_TRANSAKSI + " where id = '" + id + "' order by id desc limit 1", null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            int delete = database.delete(TABLE_TRANSAKSI, "id = '" + cursor.getString(0) + "'", new String[]{});
            if(delete == 0){
                return true;
            }
            cursor.moveToNext();
        }
        return false;
    }

    public List<String> getSolusiUmum(String id_solusi) {
        List<String> solusi = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT deskripsi FROM Solusi_Umum WHERE id_solusi = '" + id_solusi + "'", null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            solusi.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return solusi;
    }

    public List<String> getFacial(String id_solusi) {
        List<String> solusi = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT   deskripsi FROM Facial WHERE id_solusi = '" + id_solusi + "'", null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            solusi.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return solusi;
    }

    public ArrayList<String> getIdJenisFacial(String id_solusi) {
        ArrayList<String> solusi = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT id_jenis_facial FROM Facial WHERE id_solusi = '" + id_solusi + "'", null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            solusi.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return solusi;
    }

    public List<String> getProdukHarian(String id_solusi) {
        List<String> solusi = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT deskripsi FROM Produk_Harian WHERE id_solusi = '" + id_solusi + "'", null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            solusi.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();

        return solusi;
    }

}
