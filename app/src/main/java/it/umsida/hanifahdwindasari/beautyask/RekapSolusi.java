package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessGejala;
import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessRekap;

/**
 * Created by user on 31/05/2016.
 */
public class RekapSolusi extends AppCompatActivity{

    private DatabaseAccessRekap dbrekap;
    private DatabaseAccessGejala db;
    private LinearLayout lvgejalawajah, lvSolusiUmum, lvFacial, lvProdukHarian;
    private ArrayAdapter<String> gejalawajah, solusiUmum, facial, produkHarian;
    private String solusiId;
    private String date;
    ArrayList<String> col;
    private String gejala_all;
    ImageView tombolhapus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_solusi);
        ImageView tombolhapus = (ImageView) findViewById(R.id.tombol_hapus);

        db = DatabaseAccessGejala.getInstance(this);
        db.open();
        dbrekap = DatabaseAccessRekap.getInstance(this);
        dbrekap.open();

        date = getIntent().getStringExtra("date");
        HashMap datarekap = dbrekap.getDetailRekap(date);
        solusiId = datarekap.get("solusi").toString();
        gejala_all = datarekap.get("gejala").toString();
        tombolhapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //boolean hapus;
                try{
                    dbrekap.deleteRekap(date);
                }catch (SQLiteException e){
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }

                //if(hapus==true)

                Intent pindah = new Intent(RekapSolusi.this, RekapData.class);
                //pindah.putExtra("solusi-id", solusiId);
                startActivity(pindah);
                finish();

            }
        });
        col = new ArrayList<String>(Arrays.asList(gejala_all.split(",")));

        lvgejalawajah = (LinearLayout) findViewById(R.id.lv_gejala_wajah);
        lvSolusiUmum = (LinearLayout) findViewById(R.id.lv_solusi_umum);
        lvFacial = (LinearLayout) findViewById(R.id.lv_facial_wajah);
        lvProdukHarian = (LinearLayout) findViewById(R.id.lv_produk_harian);

        setData(lvgejalawajah,col,null,R.layout.simple_list_item,null);
        setData(lvProdukHarian,db.getSolusiUmum(solusiId),null,R.layout.simple_list_item,null);
        setData(lvFacial,db.getFacial(solusiId),null,R.layout.simple_list_item,null);
        setData(lvSolusiUmum,db.getProdukHarian(solusiId),null,R.layout.simple_list_item,null);
    }

    private void setData(LinearLayout layout, List<String> data, List<String> tagData, int layoutId, View.OnClickListener clickEvent) {
        int i=0;
        for(String text:data){
            TextView textView = (TextView) getLayoutInflater().inflate(layoutId,layout,false);
            textView.setText(text);
            if(tagData!=null && i<tagData.size())
                textView.setTag(tagData.get(i));

            if(clickEvent!=null){
                textView.setOnClickListener(clickEvent);
            }
            layout.addView(textView);
            i++;
        }
    }
    
    public void onBackPressed(){
       super.onBackPressed();
        finish();
        Intent pindah = new Intent(RekapSolusi.this, RekapData.class);
        startActivity(pindah);

    }

}
