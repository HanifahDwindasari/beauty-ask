package it.umsida.hanifahdwindasari.beautyask;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;

public class DetailTipsKecantikan extends AppCompatActivity {
    ScaleGestureDetector scaleGestureDetector;
    TextView detail_tipskecantikan, judultipsperawatan;
    ImageView gambar3;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tips_kecantikan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        detail_tipskecantikan = (TextView) findViewById(R.id.detail_tips);
        Intent i = getIntent();
        HashMap data = (HashMap) i.getSerializableExtra("map");
        setTitle((String) data.get("judul_tips"));
        detail_tipskecantikan.setText(Html.fromHtml(data.get("deskripsi_tips").toString()));
        //Scale
        scaleGestureDetector = new ScaleGestureDetector(this, new simpleOnScaleGestureListener());
        //
        gambar3 = (ImageView) findViewById(R.id.gambar_3);
        judultipsperawatan = (TextView) findViewById(R.id.text_judul_tips_kecantikan);

        judultipsperawatan.setText(data.get("judul_tips").toString());

        detail_tipskecantikan.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getPointerCount() == 1) {
                    //stuff for 1 pointer
                } else { //when 2 pointers are present
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            scaleGestureDetector.onTouchEvent(event);
                            break;

                        case MotionEvent.ACTION_MOVE:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            scaleGestureDetector.onTouchEvent(event);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                }
                return true;
            }
        });
        try {
            Context con = gambar3.getContext();
            switch ((String) data.get("judul_tips")) {
                case "5 Rahasia Cantik Tanpa Makeup":
                    gambar3.setImageResource(R.drawable.tips1);
                    break;
                case "12 Tips Terbaik untuk Kecantikan Alami":
                    gambar3.setImageResource(R.drawable.tips2);
                    break;
                case "3 Cara untuk Bibir Sehat dan Lembut":
                    gambar3.setImageResource(R.drawable.tips3);
                    break;
                case "4 Tips agar Kulit Selalu Bersih Sempurna":
                    gambar3.setImageResource(R.drawable.tips4);
                    break;
                case "4 Cara Jitu dan Alami Hilangkan Lingkaran Mata":
                    gambar3.setImageResource(R.drawable.tips5);
                    break;
                case "4 Tips Cara Memilih dan Memakai Sunscreen":
                    gambar3.setImageResource(R.drawable.tips6);
                    break;
                case "4 Tips Cara Mudah Kecilkan Pori-Pori yang Besar":
                    gambar3.setImageResource(R.drawable.tips7);
                    break;
                case "5 Kebiasaan yang Menyebabkan Kerusakan Kulit":
                    gambar3.setImageResource(R.drawable.tips8);
                    break;
                case "6 Tips Agar Moisturizer Lebih Maksimal Melembabkan Kulit":
                    gambar3.setImageResource(R.drawable.tips9);
                    break;
                case "6 Kesalahan yang Harus Dihindari Saat Cuci Muka":
                    gambar3.setImageResource(R.drawable.tips10);
                    break;
                case "Ini Hal Penting yang Perlu Diketahui Tentang Pelembab Wajah":
                    gambar3.setImageResource(R.drawable.tips11);
                    break;
                case "Vitamin untuk Kulit Berjerawat":
                    gambar3.setImageResource(R.drawable.tips12);
                    break;
                case "10 Tips Merawat Kulit Kencang dan Sehat":
                    gambar3.setImageResource(R.drawable.tips13);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            return;
        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailTipsKecantikan Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailTipsKecantikan Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
    //scale
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    public class simpleOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            float size = detail_tipskecantikan.getTextSize();
            Log.d("TextSizeStart", String.valueOf(size));

            //float factor = detector.getScaleFactor();
            float factor = Math.max(0.5f, Math.min(detector.getScaleFactor(), 20f));
            Log.d("Factor", String.valueOf(factor));


            float product = size*factor;
            Log.d("TextSize", String.valueOf(product));

            float safe = Math.abs(product - size);

            if(product <= 100 && product >= 1 && safe < 3){
                //tv.setText("factor= " +factor + "\n" +  "product = \n" + size + " * " + factor + " \n= " + product +"\n" + getString(R.string.hello_world));
                if (product < 18.0f){
                    detail_tipskecantikan.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                }else {
                    detail_tipskecantikan.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                }

            }

            size = detail_tipskecantikan.getTextSize();
            Log.d("TextSizeEnd", String.valueOf(size));
            return true;
        }
    }
}
