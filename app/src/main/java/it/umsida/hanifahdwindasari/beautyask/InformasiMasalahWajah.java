package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessMasalah;


/**
 * Created by user on 19/04/2016.
 */
public class InformasiMasalahWajah extends AppCompatActivity {
    private ListView masalah_wajah;

    //public static int[] images={R.drawable.jerawat, R.id}

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_masalah_wajah);
        masalah_wajah = (ListView) findViewById(R.id.list_masalah_wajah);
        final DatabaseAccessMasalah databaseAccess = DatabaseAccessMasalah.getInstance(this);
        databaseAccess.open();

        final List<String> masalahwajah = databaseAccess.getListMasalah();
        String[] mw = masalahwajah.toArray(new String[masalahwajah.size()]);
        int[] imgid = {
                R.drawable.jerawat,
                R.drawable.flek,
                R.drawable.berminyak,
                R.drawable.kerutan,
                R.drawable.kusam,
                R.drawable.kering,
        };
        //databaseAccess.close();



        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, masalahwajah);
        //
        CustomAdapterMasalahWajah adapter = new CustomAdapterMasalahWajah(this,mw, imgid);
           masalah_wajah.setAdapter(adapter);

        masalah_wajah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap detail = databaseAccess.getDetailMasalahWajah(masalahwajah.get(position));
                Intent i = new Intent(InformasiMasalahWajah.this,DetailMasalahWajahActivity.class);
                i.putExtra("map",detail);
                startActivity(i);
                //Toast.makeText(InformasiMasalahWajah.this, "Nama : "+detail.get("nama"), Toast.LENGTH_SHORT).show();
            }
        });

    }
}