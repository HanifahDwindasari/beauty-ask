package it.umsida.hanifahdwindasari.beautyask;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;

public class DetailMasalahwajah2Activity extends AppCompatActivity {
    ScaleGestureDetector scaleGestureDetector;
    TextView detail, judul;
    ImageView img;
    private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_masalahwajah2);

        detail = (TextView) findViewById(R.id.txt_detail_baru);
        Intent i = getIntent();
        HashMap data = (HashMap) i.getSerializableExtra("map");
        setTitle((String) data.get("nama"));
//        int gambar = Integer.parseInt((String)data.get("gambar"));
        detail.setText(Html.fromHtml(data.get("deskripsi").toString()));
        //Scale
        scaleGestureDetector = new ScaleGestureDetector(this, new simpleOnScaleGestureListener());
        //
        img = (ImageView) findViewById(R.id.gambar_baru);
        judul = (TextView) findViewById(R.id.txt_judul_detail_masalah_wajah);

        judul.setText(data.get("nama").toString());
        detail.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getPointerCount() == 1){
                    //stuff for 1 pointer
                }else{ //when 2 pointers are present
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            scaleGestureDetector.onTouchEvent(event);
                            break;

                        case MotionEvent.ACTION_MOVE:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            scaleGestureDetector.onTouchEvent(event);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                }
                return true;
            }
        });

        try {

            Context con = img.getContext();

            switch ((String) data.get("nama")){
                case "Jerawat" :
                    img.setImageResource(R.drawable.jerawat);
                    break;
                case"Flek" :
                    img.setImageResource(R.drawable.flek);
                    break;
                case "Berminyak" :
                    img.setImageResource(R.drawable.berminyak);
                    break;
                case "Kerutan" :
                    img.setImageResource(R.drawable.kerutan);
                    break;
                case "Kusam" :
                    img.setImageResource(R.drawable.kusam);
                    break;
                case "Kering" :
                    img.setImageResource(R.drawable.kering);
                    break;
                default:
                    break;
            }
        } catch (Exception e){
            return;
        }


        //foto

        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailMasalahWajah Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailMasalahWajah Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
    //scale
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    public class simpleOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            float size = detail.getTextSize();
            Log.d("TextSizeStart", String.valueOf(size));

            //float factor = detector.getScaleFactor();
            float factor = Math.max(0.5f, Math.min(detector.getScaleFactor(), 20f));
            Log.d("Factor", String.valueOf(factor));


            float product = size*factor;
            Log.d("TextSize", String.valueOf(product));

            float safe = Math.abs(product - size);

            if(product <= 100 && product >= 1 && safe < 3){
                //tv.setText("factor= " +factor + "\n" +  "product = \n" + size + " * " + factor + " \n= " + product +"\n" + getString(R.string.hello_world));
                if (product < 18.0f){
                    detail.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                }else {
                    detail.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                }

            }

            size = detail.getTextSize();
            Log.d("TextSizeEnd", String.valueOf(size));
            return true;
        }
    }
}
