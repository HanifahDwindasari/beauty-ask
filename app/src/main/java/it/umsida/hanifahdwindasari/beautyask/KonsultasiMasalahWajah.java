package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessGejala;

/**
 * Created by user on 16/05/2016.
 */
public class KonsultasiMasalahWajah extends AppCompatActivity {
    ViewFlipper viewFlipper;
    Button btnnext, btnprev;
    TextView pertanyaan;
    String[] ya = new String[6];
    RadioButton radioButtony;
    RadioButton radioButtont;
    RadioGroup radioGroup;
    String sql = null;
    private String key;
    String currentID;
    String prevID;
    String IDTransaksi = "1";
    DatabaseAccessGejala databaseAccessGejala;
    String currentGejala;
    boolean first = true;

    public static class Jawaban {
        String pertanyaan, id;
        boolean answertrue;
        boolean answerfalse;
        String gejala;

        public Jawaban(String id, String pertanyaan, String gejala, boolean answertrue, boolean answerfalse) {
            this.pertanyaan = pertanyaan;
            this.answertrue = answertrue;
            this.answerfalse = answerfalse;
            this.id = id;
            this.gejala = gejala;
        }
    }
//menampung jawabannya
    List<Jawaban> jawaban = new ArrayList<>();
    ArrayList<String> collection = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String uuid = UUID.randomUUID().toString();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        databaseAccessGejala = DatabaseAccessGejala.getInstance(this);
        databaseAccessGejala.open();

        setContentView(R.layout.konsultasi_masalah_wajah);

        btnnext = (Button) findViewById(R.id.next);
        btnprev = (Button) findViewById(R.id.prev);
        radioButtony = (RadioButton) findViewById(R.id.radioJawaby);
        radioButtont = (RadioButton) findViewById(R.id.radioJawabt);
        pertanyaan = (TextView) findViewById(R.id.pertanyaan1);

        tampilkanGejala();
        /*
        radioButtony.setChecked(false);
        radioButtont.setChecked(false);
*/
        btnprev.setVisibility(View.GONE);

        //jawaban disimpan didalam list jawaban
        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String jawaban = "t";
                if (!radioButtont.isChecked() && !radioButtony.isChecked()) {
                    // jika tidak ada jawaban terpilih, maka tampilkan pesan peringatan
                    Toast.makeText(getApplicationContext(), "Harus dipilih", Toast.LENGTH_SHORT).show();
                } else {
                    // setiap jawaban akan disimpan sebagai history konsultasi
                    // tujuannya, agar pengguna dapat kembali ke pertanyaan sebelumnya dengan jawaban yang pernah pengguna pilih
                    if (radioButtony.isChecked()) {

                        jawaban = "y";
                        // menyimpan jawaban pengguna
                        KonsultasiMasalahWajah.this.jawaban.add(new Jawaban(IDTransaksi, pertanyaan.getText().toString(), currentGejala, radioButtony.isChecked(), radioButtont.isChecked()));

                    } else if (radioButtont.isChecked()) {
                        jawaban = "t";
                        // menyimpan jawaban pengguna
                        KonsultasiMasalahWajah.this.jawaban.add(new Jawaban(IDTransaksi, pertanyaan.getText().toString(), currentGejala, radioButtony.isChecked(), radioButtont.isChecked()));

                    }

                    // mengambil data selanjutnya
                    // pada tahap ini, data yang diambil ada 2 macam, data yang merupakan GEJALA dan SOLUSI
                    // jika data merupakan GEJALA, maka tampilkan gejala
                    // jika data merupakan SOLUSI, buka halaman yang menampilkan SOLUSI
                    HashMap gejalaBerikutnya = databaseAccessGejala.getGejalaBerikutnya(IDTransaksi, jawaban);
                    if (gejalaBerikutnya.get("status").toString().equals("gejala")) {
                        // data merupakan gejala, tampilkan gejala
                        currentID = gejalaBerikutnya.get("id_gejala").toString();
                        IDTransaksi = gejalaBerikutnya.get("id").toString();

                        tampilkanGejala();
                        radioButtony.setChecked(true);


                    } else {
                        // data selanjutnya merupakan solusi
                        for (int i = 0; i < KonsultasiMasalahWajah.this.jawaban.size(); i++) {
                            Jawaban jaw = KonsultasiMasalahWajah.this.jawaban.get(i);
                            if (jaw.answertrue == true) {
                                collection.add(jaw.gejala);
                                //jawaban ya ditampung
                            }
                        }
                        // ambil deskripsi solusi dari {gejalaBerikutnya}
                        String solusi_id = gejalaBerikutnya.get("solusi").toString();
                        Intent intent = new Intent(KonsultasiMasalahWajah.this, SolusiMasalah.class);
                        // tutup halaman ini
                        finish();
                        // kirim solusi dan juga rekap gejala ke halaman SolusiMasalah
                        intent.putExtra("solusi-id", solusi_id);
                        intent.putStringArrayListExtra("selected", collection);

                        startActivity(intent);
                    }

                    if (KonsultasiMasalahWajah.this.jawaban.size() == 0) {
                        btnprev.setVisibility(View.GONE);
                    } else {
                        btnprev.setVisibility(View.VISIBLE);
                    }

                }
                System.out.println(data());
            }
        });
        btnprev.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (jawaban.size() != 0) {
                            //selama ada jawaban, meremove jawaban terakhir dan diganti dengan id jawaban selanjutnya
                            Jawaban j = jawaban.remove(jawaban.size() - 1);
                            IDTransaksi = j.id;
                            currentID = j.gejala;
                            currentGejala = j.gejala;
                            pertanyaan.setText(j.pertanyaan);
                            radioButtony.setChecked(j.answertrue);
                            radioButtont.setChecked(j.answerfalse);
                            if (j.answertrue) {
                                radioButtony.setChecked(j.answertrue);
                            } else {
                                radioButtont.setChecked(true);
                            }
                        }

                        System.out.println(data());
                        if (jawaban.size() == 0) {
                            btnprev.setVisibility(View.GONE);
                        } else {
                            btnprev.setVisibility(View.VISIBLE);
                        }
                    }
                }
        );
    }

    private void tampilkanGejala() {
        try {
            HashMap gejala = databaseAccessGejala.getGejala(currentID);
            pertanyaan.setText(gejala.get("nama_gejala").toString());

            currentID = gejala.get("id_gejala").toString();
            currentGejala = gejala.get("deskripsi").toString();
        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
/*
    MySQLHelper dbHelper;
    private Cursor cursor;
    private SimpleCursorAdapter adapter;

    private void view(){
        dbHelper = new MySQLHelper(getApplicationContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        try{
            cursor = db.rawQuery(sql, null);
            cursor.moveToFirst();
            int i = -1;
            do{
                i++;
                pertanyaan[i].setText(cursor.getString(cursor.getColumnIndex("pertanyaan")));
                jawaban[i][0].setText(cursor.getString(cursor.getColumnIndex("ya")));
                jawaban[i][1].setText(cursor.getString(cursor.getColumnIndex("tidak")));

            }while (cursor.moveToNext());
            db.close();
        }catch (Exception e){
            Log.d("Exception", e.toString());
        }
    }
*/

    public String data() {
        String b = "";
        for (Jawaban j : jawaban) {
            b += j.gejala + " : " + j.answertrue + ",";
        }
        return b;
    }
}