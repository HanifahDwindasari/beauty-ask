package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessMasalah;

public class NavDrawerMasalahWajah extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView masalah_wajah;

    DatabaseAccessMasalah databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer_masalah_wajah);
        masalah_wajah = (ListView) findViewById(R.id.list_masalah_wajah);
        databaseAccess = DatabaseAccessMasalah.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        databaseAccess.open();

        final List<String> masalahwajah = databaseAccess.getListMasalah();
        String[] mw = masalahwajah.toArray(new String[masalahwajah.size()]);
        int[] imgid = {
                R.drawable.jerawat,
                R.drawable.flek,
                R.drawable.berminyak,
                R.drawable.kerutan,
                R.drawable.kusam,
                R.drawable.kering,
        };
        CustomAdapterMasalahWajah adapter = new CustomAdapterMasalahWajah(this, mw, imgid);
        masalah_wajah.setAdapter(adapter);

        masalah_wajah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap detail = databaseAccess.getDetailMasalahWajah(masalahwajah.get(position));
                Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
                i.putExtra("map", detail);
                startActivity(i);
                //Toast.makeText(InformasiMasalahWajah.this, "Nama : "+detail.get("nama"), Toast.LENGTH_SHORT).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer_masalah_wajah, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_berminyak) {
            HashMap detail = databaseAccess.getDetailMasalahWajah("Berminyak");
            Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_flek) {
            HashMap detail = databaseAccess.getDetailMasalahWajah("Flek");
            Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_jerawat) {
            HashMap detail = databaseAccess.getDetailMasalahWajah("Jerawat");
            Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_kering) {
            HashMap detail = databaseAccess.getDetailMasalahWajah("Kering");
            Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
            i.putExtra("map", detail);
            startActivity(i);

        } else if (id == R.id.nav_kerutan) {
            HashMap detail = databaseAccess.getDetailMasalahWajah("Kerutan");
            Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_kusam) {
            HashMap detail = databaseAccess.getDetailMasalahWajah("Kusam");
            Intent i = new Intent(NavDrawerMasalahWajah.this, DetailMasalahwajah2Activity.class);
            i.putExtra("map", detail);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
