package it.umsida.hanifahdwindasari.beautyask.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 25/04/2016.
 */
public class DatabaseAccessFacial {
    private static final String TABLE_NAME = "Jenis_Facial";
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccessFacial instance;

    private DatabaseAccessFacial(Context context){
        this.openHelper = new DatabaseOpenHelper(context);
    }
    public static DatabaseAccessFacial getInstance(Context context){
        if (instance == null){
            instance = new DatabaseAccessFacial(context);
        }
        return instance;
    }
    public void open(){
        this.database = openHelper.getWritableDatabase();
    }
    public  void close(){
        if (database != null){
            this.database.close();
        }
    }
    public List<String> getListFacial(){
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT jenis_perawatan FROM "+TABLE_NAME, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
    public HashMap getDetailFacial(String facial){
        HashMap detailfacial = new HashMap();
        Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE jenis_perawatan="+"'"+facial+"'", null);
        //Log.d("db", "getDetailFacial: "+cursor.getColumnCount());
        cursor.moveToFirst();
        detailfacial.put("id_facial",cursor.getString(0));
        detailfacial.put("jenis_perawatan",cursor.getString(1));
        detailfacial.put("deskripsi_facial",cursor.getString(2));
        detailfacial.put("aturan_facial",cursor.getString(3));
        detailfacial.put("keterangan1", cursor.getString(4));
        detailfacial.put("id_jenis_facial", cursor.getString(5));
        detailfacial.put("keterangan2", cursor.getString(6));
        detailfacial.put("keterangan3", cursor.getString(7));
        detailfacial.put("keterangan4", cursor.getString(8));
        detailfacial.put("keterangan5", cursor.getString(9));
        detailfacial.put("keterangan6", cursor.getString(10));
        detailfacial.put("keterangan7", cursor.getString(11));
        detailfacial.put("keterangan8", cursor.getString(12));
        detailfacial.put("keterangan9", cursor.getString(13));
        detailfacial.put("harga", cursor.getString(14));
        cursor.close();
        return detailfacial;
    }

    public List<String> getIdJenisFacial(String deskripsi){
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT id_jenis_facial FROM"+TABLE_NAME+" WHERE deskripsi ='"+deskripsi+"'",null);
        cursor.moveToFirst();
        list.add(cursor.getString(0));
        //String sid = "Tes";
        //int id = Integer.parseInt(sid);
        cursor.close();
        return list;
    }

    public String getDeskripsi(int id){
        Cursor cursor = database.rawQuery("SELECT deskripsi_facial FROM Jenis_Facial WHERE id_facial='"+id+"'",null);
        cursor.moveToFirst();
        String deskripsi = cursor.getString(0).toString();
        cursor.close();
        return deskripsi;
    }

    public HashMap<String, String> getDetail(String id_jenis_facial) {
        HashMap<String, String> data = new HashMap<>();
        //Cursor cursor = database.rawQuery("SELECT deskripsi_facial, aturan_facial FROM Jenis_Facial WHERE id_jenis_facial = '" + id_jenis_facial + "'", null);
        Cursor cursor = database.rawQuery("SELECT jenis_perawatan, aturan_facial FROM Jenis_Facial WHERE id_jenis_facial = '" + id_jenis_facial + "'", null);
        cursor.moveToFirst();

        String jenis = (null == cursor.getString(0) ? "" : cursor.getString(0));
        String aturan = (null == cursor.getString(1) ? "" : cursor.getString(1));

        data.put("jenis", jenis);
        data.put("aturan", aturan);

        return data;
    }
}
