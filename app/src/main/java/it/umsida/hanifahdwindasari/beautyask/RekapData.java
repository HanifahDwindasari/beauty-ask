package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessRekap;

/**
 * Created by user on 31/05/2016.
 */
public class RekapData extends AppCompatActivity {
    ListView list_data;
    private DatabaseAccessRekap dbrekap;
    private List<String> mRekap;
    ArrayAdapter<String> list;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        list_data = (ListView) findViewById(R.id.list_date);

        dbrekap = DatabaseAccessRekap.getInstance(this);
        dbrekap.open();

        mRekap = dbrekap.getListRekap();

        list = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, mRekap );

        list_data.setAdapter(list);
        list_data.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent pindah = new Intent(RekapData.this, RekapSolusi.class);
                pindah.putExtra("date", list.getItem(position));
                startActivity(pindah);
                finish();
            }
        });

    }
}

