package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessFacial;

/**
 * Created by user on 25/04/2016.
 */
public class InformasiFacialWajah extends AppCompatActivity {
    private ListView facial_wajah;
    private List<String> items;
    private Menu menu;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_facial_wajah);
        facial_wajah = (ListView) findViewById(R.id.list_facial_wajah);
        final DatabaseAccessFacial databaseAccess = DatabaseAccessFacial.getInstance(this);
        databaseAccess.open();

        //search
//        getMenuInflater().inflate(R.menu.nav_drawer_facial_wajah, menu);
//        this.menu = menu;
//            SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//            SearchView search = (SearchView) menu.findItem(R.id.pencarian2).getActionView();
//            search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
//            search.setOnQueryTextListener(String query) {
//                loadHistory(query);
//                return true;
//            }




        final List<String> facialwajah = databaseAccess.getListFacial();
        String[] fw = facialwajah.toArray(new String[facialwajah.size()]);
        int[] imgid = {
                R.drawable.facial1,
                R.drawable.facial2,
                R.drawable.facial3,
                R.drawable.facial4,
                R.drawable.facial5,
                R.drawable.facial6,
                R.drawable.facial7,
                R.drawable.facial8,
                R.drawable.facial9,
                R.drawable.facial10,
                R.drawable.facial11,
                R.drawable.facial12,
                R.drawable.facial13,
                R.drawable.facial14,
                R.drawable.facial15,
                R.drawable.facial16,
                R.drawable.facial17,
                R.drawable.facial18,
                R.drawable.facial19,
                R.drawable.facial20,
                R.drawable.facial21,
                R.drawable.facial22,
                R.drawable.facial23,
                R.drawable.facial24,
                R.drawable.facial25,
                R.drawable.facial26,
                R.drawable.facial27,
                R.drawable.facial28,
                R.drawable.facial29,

        };
        //databaseAccess.close();

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, facialwajah);
        CustomAdapterFacialWajah adapter = new CustomAdapterFacialWajah(this, fw, imgid);
        facial_wajah.setAdapter(adapter);


        facial_wajah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap detail = databaseAccess.getDetailFacial(facialwajah.get(position));
                Intent i = new Intent(InformasiFacialWajah.this,DetailFacialWajahs.class);
                i.putExtra("map",detail);
                startActivity(i);
                //Toast.makeText(InformasiMasalahWajah.this, "Nama : "+detail.get("nama"), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
