package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessTipsKecantikan;

public class NavDrawerTipsKecantikan extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView tips_kecantikan;

    DatabaseAccessTipsKecantikan databaseAccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer_tips_kecantikan);
        tips_kecantikan = (ListView) findViewById(R.id.list_tips_kecantikan);

        databaseAccess = DatabaseAccessTipsKecantikan.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        databaseAccess.open();

        final List<String> tipskecantikan = databaseAccess.getListTipsKecantikan();
        String[] tk = tipskecantikan.toArray(new String[tipskecantikan.size()]);
        int[] imgid = {
                R.drawable.tips1,
                R.drawable.tips2,
                R.drawable.tips3,
                R.drawable.tips4,
                R.drawable.tips5,
                R.drawable.tips6,
                R.drawable.tips7,
                R.drawable.tips8,
                R.drawable.tips9,
                R.drawable.tips10,
                R.drawable.tips11,
                R.drawable.tips12,
                R.drawable.tips13,
        };
        CustomAdapterTipsKecantikan adapter = new CustomAdapterTipsKecantikan(this, tk, imgid);
        tips_kecantikan.setAdapter(adapter); //control item mulai dari view, sumber itemnya

        tips_kecantikan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //mengambil posisi item di list view
                HashMap detail = databaseAccess.getDetailTipsKecantikan(tipskecantikan.get(position));
                Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
                i.putExtra("map", detail);
                startActivity(i);
                //Toast.makeText(InformasiMasalahWajah.this, "Nama : "+detail.get("nama"), Toast.LENGTH_SHORT).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(  //buka tutup navigasi drawer
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tips1) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("5 Rahasia Cantik Tanpa Makeup");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips2) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("12 Tips Terbaik untuk Kecantikan Alami");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips3) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("3 Cara untuk Bibir Sehat dan Lembut");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips4) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("4 Tips agar Kulit Selalu Bersih Sempurna");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips5) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("4 Cara Jitu dan Alami Hilangkan Lingkaran Mata");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips6) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("4 Tips Cara Memilih dan Memakai Sunscreen");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        }else if (id == R.id.nav_tips7) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("4 Tips Cara Mudah Kecilkan Pori-Pori yang Besar");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips8) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("5 Kebiasaan yang Menyebabkan Kerusakan Kulit");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips9) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("6 Tips Agar Moisturizer Lebih Maksimal Melembabkan Kulit");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips10) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("6 Kesalahan yang Harus Dihindari Saat Cuci Muka");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips11) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("Ini Hal Penting yang Perlu Diketahui Tentang Pelembab Wajah");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips12) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("Vitamin untuk Kulit Berjerawat");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        } else if (id == R.id.nav_tips13) {
            HashMap detail = databaseAccess.getDetailTipsKecantikan("10 Tips Merawat Kulit Kencang dan Sehat");
            Intent i = new Intent(NavDrawerTipsKecantikan.this, DetailTipsKecantikan.class);
            i.putExtra("map", detail);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
