package it.umsida.hanifahdwindasari.beautyask;

import android.app.SearchManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

public class SearchFacialWajah extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_facial_wajah);
        getSupportActionBar().setTitle("pencarian");
        handleIntent(getIntent());
   }
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.nav_drawer_facial_wajah, menu);

        return true;
    }
    @Override
    protected void onNewIntent (Intent intent){
        super.onNewIntent(intent);
        handleIntent(intent);
    }
    private void handleIntent(Intent intent){
        if(Intent.ACTION_SEARCH.equals(intent.getAction())){
            String query = intent.getStringExtra(SearchManager.QUERY);
            Toast.makeText(SearchFacialWajah.this, query, Toast.LENGTH_SHORT).show();
        }
    }
}
