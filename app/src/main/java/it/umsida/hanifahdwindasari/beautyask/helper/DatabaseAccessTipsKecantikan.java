package it.umsida.hanifahdwindasari.beautyask.helper;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 25/04/2016.
 */
public class DatabaseAccessTipsKecantikan {
    private static final String TABLE_NAME = "Tips_Kecantikan";
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccessTipsKecantikan instance;

    private DatabaseAccessTipsKecantikan(Context context){
        this.openHelper = new DatabaseOpenHelper(context);
    }
    //membuat objek database tips kecantikan
    public static DatabaseAccessTipsKecantikan getInstance(Context context){
        if (instance == null){
            instance = new DatabaseAccessTipsKecantikan(context);
        }
        return instance;
    }
    //untuk mengambil data
    public void open(){

        this.database = openHelper.getWritableDatabase();
    }
    public  void close(){
        if (database != null){
            this.database.close();
        }
    }
    public List<String> getListTipsKecantikan(){
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT judul_tips FROM "+TABLE_NAME, null);
        cursor.moveToFirst(); //iterasi pertama baris pertama
        while (!cursor.isAfterLast()){
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public HashMap getDetailTipsKecantikan(String kecantikan){
        HashMap detail = new HashMap();
        Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE judul_tips="+"'"+kecantikan+"'", null);
        cursor.moveToFirst();
        detail.put("id_tips",cursor.getString(0));
        detail.put("judul_tips",cursor.getString(1));
        detail.put("deskripsi_tips",cursor.getString(2));
        detail.put("gambar_tips",cursor.getString(3));
        cursor.close();
        return detail;
    }
}