package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessFacial;
import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessGejala;
import it.umsida.hanifahdwindasari.beautyask.helper.DatabaseAccessRekap;

public class SolusiMasalah extends AppCompatActivity {
    ImageView tombol_save;
    DatabaseAccessFacial databaseAccess;
    private LinearLayout lvgejalawajah, lvSolusiUmum, lvFacial, lvProdukHarian;
    private TextView tvFacial;
    private ArrayAdapter<String> gejalawajah, solusiUmum, facial, produkHarian;
    private ArrayList<String> idJenisFacial = new ArrayList<>();
    private String solusiId;
    private DatabaseAccessGejala db;
    private DatabaseAccessFacial dbFacial;
    private DatabaseAccessRekap dbrekap;
    ArrayList<String> col;
    Button Close, Create;
    private PopupWindow pw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solusi_masalah);
        ImageView tombol_save = (ImageView) findViewById(R.id.tombol_simpan);
        databaseAccess = DatabaseAccessFacial.getInstance(this);
        dbFacial = DatabaseAccessFacial.getInstance(this);
        dbFacial.open();
        db = DatabaseAccessGejala.getInstance(this);
        db.open();
        dbrekap = DatabaseAccessRekap.getInstance(this);
        dbrekap.open();

        solusiId = getIntent().getStringExtra("solusi-id");
        col = getIntent().getStringArrayListExtra("selected");

        lvgejalawajah = (LinearLayout) findViewById(R.id.lv_gejala_wajah);
        lvSolusiUmum = (LinearLayout) findViewById(R.id.lv_solusi_umum);
        lvFacial = (LinearLayout) findViewById(R.id.lv_facial_wajah);
        lvProdukHarian = (LinearLayout) findViewById(R.id.lv_produk_harian);

        tampilkanSolusi();
        // ketika tombol save ditekan, simpan rekap data
        tombol_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long request = simpan_data(solusiId, col);
                if (request > 0) {
                    Toast.makeText(getApplicationContext(), "Simpan Berhasil!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Simpan Gagal!", Toast.LENGTH_SHORT).show();
                }
                //Intent pindah = new Intent(SolusiMasalah.this, MainActivity.class);
                //pindah.putExtra("solusi-id", solusiId);
                //indah.putStringArrayListExtra("selected", col);
                //startActivity(pindah);
                finish();
            }
        });
    }


    private void showPopup(String deskripsi_facial, String aturan_facial) {
        //HashMap detail = databaseAccess.getDetailFacial(deskripsi_facial);
        Toast.makeText(SolusiMasalah.this, deskripsi_facial , Toast.LENGTH_SHORT).show();
//        Intent i = new Intent(SolusiMasalah.this,TesActivity.class);
//        i.putExtra("map",detail);
//        startActivity(i);
        //Toast.makeText(SolusiMasalah.this, deskripsi_facial + " " + aturan_facial, Toast.LENGTH_SHORT).show();
//        try {
//            final Dialog dialog = new Dialog(this);
//            dialog.setContentView(R.layout.popup);
//
//            TextView txt = (TextView) dialog.findViewById(R.id.txtView);
//            TextView txt2 = (TextView) dialog.findViewById(R.id.txtView2);
//            txt.setText(deskripsi_facial);
//            txt2.setText(aturan_facial);
//
//            Close = (Button) dialog.findViewById(R.id.close_popup);
//            Close.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//                }
//            });
//
//            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
//            dialog.show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    private long simpan_data(String solusiId, ArrayList<String> col) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        // simpan WAKTU pembuatan rekap
        String date = sdf.format(c.getTime());

        // mengambil semua data GEJALA kemudian digabungkan menjadi 1 KALIMAT
        String gejala = "";
        for (int i = 0; i < col.size(); i++) {
            gejala += col.get(i).toString();
            if (i != col.size() - 1)
                gejala += ",";
        }

        // simpan rekap pada DATABASE
        long request = dbrekap.addRekap(date, gejala, solusiId);
        return request;
    }

    private void tampilkanSolusi() {
//        gejalawajah = new ArrayAdapter<String>(this, R.layout.simple_list_item, col);
//        solusiUmum = new ArrayAdapter<String>(this, R.layout.simple_list_item, db.getSolusiUmum(solusiId));
//        facial = new ArrayAdapter<String>(this, R.layout.simple_list_item, db.getFacial(solusiId));
//        produkHarian = new ArrayAdapter<String>(this, R.layout.simple_list_item, db.getProdukHarian(solusiId));
//
        idJenisFacial = db.getIdJenisFacial(solusiId);
//
//        lvgejalawajah.setAdapter(gejalawajah);
//        lvSolusiUmum.setAdapter(solusiUmum);
//        lvProdukHarian.setAdapter(produkHarian);
//
//        lvFacial.setAdapter(facial);
//
//        lvFacial.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                //String id_jenis_facial = dbFacial.getIdJenisFacial(nama);
//                /*String deskripsi_facial = dbFacial.getDeskripsi(id_jenis_facial);
//                showPopup(deskripsi_facial)*/
//                //System.out.println(id_jenis_facial);
//                showDetail(position);
//            }
//        });

        setData(lvgejalawajah, col,null, R.layout.simple_list_item,null);
        setData(lvProdukHarian,db.getProdukHarian(solusiId),null,R.layout.simple_list_item,null);
        setData(lvFacial, db.getFacial(solusiId),idJenisFacial, R.layout.simple_list_item, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v instanceof TextView){
                    showDetail(((TextView)v).getTag().toString());
                }
            }
        });
        setData(lvSolusiUmum,db.getSolusiUmum(solusiId),null,R.layout.simple_list_item,null);
    }


    @Override
    //agar tidak mencreate activity baru
    public void onBackPressed() {
        Intent pindah = new Intent(SolusiMasalah.this, Konsultasi.class);
        pindah.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(pindah);
        finish();
    }

    //menampilkan semua list datanya. data kedalam list
    private void setData(LinearLayout layout,List<String> data,List<String> tagData,int layoutId,View.OnClickListener clickEvent) {
        int i=0;
        for(String text:data){
            TextView textView = (TextView) getLayoutInflater().inflate(layoutId,layout,false);
            textView.setText(text);
            if(tagData!=null && i<tagData.size())
                textView.setTag(tagData.get(i));

            if(clickEvent!=null){
                textView.setOnClickListener(clickEvent);
            }
            layout.addView(textView);
            i++;
        }
    }

    private void showDetail(String idFacial) {

        HashMap<String, String> detail = dbFacial.getDetail(idFacial);
        HashMap<String, String> detail2 = dbFacial.getDetail(idFacial);
        //Toast.makeText(SolusiMasalah.this, detail.get("jenis")+" "+ detail2.get("aturan"), Toast.LENGTH_SHORT).show();
        //showPopup(detail.get("jenis"), detail2.get("aturan"));
        showPopup(detail.get("jenis"));

    }

    private void showPopup(String jenis) {
        HashMap detail = databaseAccess.getDetailFacial(jenis.toString());
//        Toast.makeText(SolusiMasalah.this, jenis , Toast.LENGTH_SHORT).show();
        Intent i = new Intent(SolusiMasalah.this,DetailFacialWajahs.class);
        i.putExtra("map",detail);
        startActivity(i);
    }
}