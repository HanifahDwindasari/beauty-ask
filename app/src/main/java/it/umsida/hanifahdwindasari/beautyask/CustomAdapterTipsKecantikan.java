package it.umsida.hanifahdwindasari.beautyask;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by user on 12/05/2016.
 */
public class CustomAdapterTipsKecantikan extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] nama_tipskecantikan;
    private final int[] imgid;

    public CustomAdapterTipsKecantikan(Activity context, String[] nama_tipskecantikan, int[] imgid) {
        super(context, R.layout.list_info_tips_kecantikan, nama_tipskecantikan);
        // TODO Auto-generated constructor stub

        this.context = context;
        this.nama_tipskecantikan = nama_tipskecantikan;
        this.imgid = imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.list_info_tips_kecantikan, null,true);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView3);
        TextView tv_nama_tips_kecantikan = (TextView) rowView.findViewById(R.id.textView7);
        TextView tv_lihat_penjelasan = (TextView) rowView.findViewById(R.id.textView8);

        tv_nama_tips_kecantikan.setText(nama_tipskecantikan[position]);
        tv_lihat_penjelasan.setText("LIHAT PENJELASAN");
        imageView.setImageResource(imgid[position]);
        return rowView;

    };
}
