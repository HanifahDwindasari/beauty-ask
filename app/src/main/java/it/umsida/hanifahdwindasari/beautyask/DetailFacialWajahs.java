package it.umsida.hanifahdwindasari.beautyask;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.HashMap;

public class DetailFacialWajahs extends AppCompatActivity {
    RelativeLayout layout;
    ScaleGestureDetector scaleGestureDetector, scaleGestureDetector2;
    TextView aturan_facial, harga, judul, detail_facial, keterangan1, keterangan2, keterangan3, keterangan4, keterangan5, keterangan6, keterangan7, keterangan8, keterangan9;
    ImageView gambar_utama, gambar1,gambar2, gambar3,gambar4,gambar5,gambar6,gambar7,gambar8,gambar9;
    private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_facial_wajah);
    layout = (RelativeLayout) findViewById(R.id.layout);
        detail_facial = (TextView) findViewById(R.id.detail_facial);
        keterangan1 = (TextView) findViewById(R.id.keterangan1);
        keterangan2 = (TextView) findViewById(R.id.keterangan2);
        keterangan3 = (TextView) findViewById(R.id.keterangan3);
        keterangan4 = (TextView) findViewById(R.id.keterangan4);
        keterangan5 = (TextView) findViewById(R.id.keterangan5);
        keterangan6 = (TextView) findViewById(R.id.keterangan6);
        keterangan7 = (TextView) findViewById(R.id.keterangan7);
        keterangan8 = (TextView) findViewById(R.id.keterangan8);
        keterangan9 = (TextView) findViewById(R.id.keterangan9);
        aturan_facial = (TextView) findViewById(R.id.aturan_pakai);
        harga = (TextView) findViewById(R.id.harga_facial);

        Intent i = getIntent();
        HashMap data = (HashMap) i.getSerializableExtra("map");
        setTitle((String) data.get("jenis_perawatan"));
//        int gambar = Integer.parseInt((String)data.get("gambar"));
        detail_facial.setText(Html.fromHtml(data.get("deskripsi_facial").toString()));
        //Scale
       scaleGestureDetector = new ScaleGestureDetector(this, new simpleOnScaleGestureListener());
        scaleGestureDetector2 = new ScaleGestureDetector(this, new simpleOnScaleGestureListener());
        //
        gambar_utama = (ImageView) findViewById(R.id.gambar_utama);
        gambar1 = (ImageView) findViewById(R.id.gambarfacial1);
        gambar2 = (ImageView) findViewById(R.id.gambarfacial2);
        gambar3 = (ImageView) findViewById(R.id.gambarfacial3);
        gambar4 = (ImageView) findViewById(R.id.gambarfacial4);
        gambar5 = (ImageView) findViewById(R.id.gambarfacial5);
        gambar6 = (ImageView) findViewById(R.id.gambarfacial6);
        gambar7 = (ImageView) findViewById(R.id.gambarfacial7);
        gambar8 = (ImageView) findViewById(R.id.gambarfacial8);
        gambar9 = (ImageView) findViewById(R.id.gambarfacial9);
        judul = (TextView) findViewById(R.id.text_judul_facial);

        judul.setText(data.get("jenis_perawatan").toString());
        aturan_facial.setText(Html.fromHtml(data.get("aturan_facial").toString()));
        harga.setText(Html.fromHtml(data.get("harga").toString()));

        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getPointerCount() == 1){
                    //stuff for 1 pointer
                }else{ //when 2 pointers are present
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            scaleGestureDetector.onTouchEvent(event);
                            break;

                        case MotionEvent.ACTION_MOVE:
                            // Disallow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            scaleGestureDetector.onTouchEvent(event);
                            break;

                        case MotionEvent.ACTION_UP:
                            // Allow ScrollView to intercept touch events.
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }

                }
                return true;
            }
        });

        try {

            Context con = gambar_utama.getContext();

            switch ((String) data.get("jenis_perawatan")){
                case "Totok Aura Wajah" :
                    gambar_utama.setImageResource(R.drawable.facial1);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.totok_aura_wajah);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.pembersihan_wajah_kembali);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.masker_wajah);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setVisibility(View.GONE);
                    keterangan5.setVisibility(View.GONE);
                    gambar6.setVisibility(View.GONE);
                    keterangan6.setVisibility(View.GONE);
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case"Facial Standart" :
                    gambar_utama.setImageResource(R.drawable.facial2);
                    gambar1.setImageResource(R.drawable.facial_s1);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.facial_s2);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.facial_s3);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setVisibility(View.GONE);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.facial_s5);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.facial_s5);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.facial_s6);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Fruitty" :
                    gambar_utama.setImageResource(R.drawable.facial3);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Tea Tree" :
                    gambar_utama.setImageResource(R.drawable.facial4);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Whitening" :
                    gambar_utama.setImageResource(R.drawable.facial5);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Collagen" :
                    gambar_utama.setImageResource(R.drawable.facial6);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Detox" :
                    gambar_utama.setImageResource(R.drawable.facial7);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_karbon_aktif);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case"Facial Anti Aging" :
                    gambar_utama.setImageResource(R.drawable.facial8);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Glowing Gold" :
                    gambar_utama.setImageResource(R.drawable.facial9);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Acne Premium" :
                    gambar_utama.setImageResource(R.drawable.facial10);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.masage_wajah);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.proses_penguapan);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial H&M Lightening" :
                    gambar_utama.setImageResource(R.drawable.facial11);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial H&M Acne" :
                    gambar_utama.setImageResource(R.drawable.facial12);
                    gambar1.setImageResource(R.drawable.langkah_hma1);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.langkah_hma2);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.langkah_hma3);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.langkah_hma4);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.langkah_hma5);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.langkah_hma6);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.langkah_hma7);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial H&M Sensitive" :
                    gambar_utama.setImageResource(R.drawable.facial13);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.masage_wajah);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.proses_penguapan);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);

                    break;
                case"Facial H&M Oily Skin" :
                    gambar_utama.setImageResource(R.drawable.facial14);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.masker_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Sensitive" :
                    gambar_utama.setImageResource(R.drawable.facial15);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.masage_wajah);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.proses_penguapan);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.masker_wajah);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Diamond Premium" :
                    gambar_utama.setImageResource(R.drawable.facial16);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Glowing Gold Premium" :
                    gambar_utama.setImageResource(R.drawable.facial17);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_bahan_kimi);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.penyemprotan_ozon);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setImageResource(R.drawable.pengolesan_serum);
                    keterangan9.setText(Html.fromHtml(data.get("keterangan9").toString()));
                    break;
                case "Facial Anti Aging Premium" :
                    gambar_utama.setImageResource(R.drawable.facial18);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_bahan_kimi);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.penyemprotan_ozon);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setImageResource(R.drawable.pengolesan_serum);
                    keterangan9.setText(Html.fromHtml(data.get("keterangan9").toString()));
                    break;
                case "Facial Radical White" :
                    gambar_utama.setImageResource(R.drawable.facial19);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.rf_needling);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case"Facial Mesotherapy Whitening" :
                    gambar_utama.setImageResource(R.drawable.facial20);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Facial Mesotherapy Acne" :
                    gambar_utama.setImageResource(R.drawable.facial21);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Chemical Peeling" :
                    gambar_utama.setImageResource(R.drawable.facial22);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_bahan_kimi);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.penyemprotan_ozon);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan9.setText(Html.fromHtml(data.get("keterangan9").toString()));
                    break;
                case "Derma Peel H&M" :
                    gambar_utama.setImageResource(R.drawable.facial23);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_bahan_kimi);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.penyemprotan_ozon);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Mikrodermabrasi" :
                    gambar_utama.setImageResource(R.drawable.facial24);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setVisibility(View.GONE);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.penyemprotan_ozon);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setImageResource(R.drawable.pengolesan_serum);
                    keterangan9.setText(Html.fromHtml(data.get("keterangan9").toString()));
                    break;
                case"Cauter" :
                    gambar_utama.setImageResource(R.drawable.facial25);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.cauter);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setVisibility(View.GONE);
                    keterangan4.setVisibility(View.GONE);
                    gambar5.setVisibility(View.GONE);
                    keterangan5.setVisibility(View.GONE);
                    gambar6.setVisibility(View.GONE);
                    keterangan6.setVisibility(View.GONE);
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "RF Lifting Wajah" :
                    gambar_utama.setImageResource(R.drawable.facial26);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.rf);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.masker_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan9.setText(Html.fromHtml(data.get("keterangan9").toString()));
                    break;
                case "Derma Roller" :
                    gambar_utama.setImageResource(R.drawable.facial27);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.proses_scrubing);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.masage_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.proses_penguapan);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengambilan_komedo);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setImageResource(R.drawable.pengolesan_serum);
                    keterangan6.setText(Html.fromHtml(data.get("keterangan6").toString()));
                    gambar7.setImageResource(R.drawable.masker_wajah);
                    keterangan7.setText(Html.fromHtml(data.get("keterangan7").toString()));
                    gambar8.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan8.setText(Html.fromHtml(data.get("keterangan8").toString()));
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Derma Filler" :
                    gambar_utama.setImageResource(R.drawable.facial28);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.masage_wajah);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    gambar3.setImageResource(R.drawable.menggunakan_pelembab_wajah);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.derma_filler);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setVisibility(View.GONE);
                    keterangan5.setVisibility(View.GONE);
                    gambar6.setVisibility(View.GONE);
                    keterangan6.setVisibility(View.GONE);
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                case "Botox" :
                    gambar_utama.setImageResource(R.drawable.facial29);
                    gambar1.setImageResource(R.drawable.pembersihan_wajah);
                    keterangan1.setText(Html.fromHtml(data.get("keterangan1").toString()));
                    gambar2.setImageResource(R.drawable.masage_wajah);
                    keterangan2.setText(Html.fromHtml(data.get("keterangan2").toString()));
                    // gambar 3 gak pake
                    gambar3.setVisibility(View.GONE);
                    keterangan3.setText(Html.fromHtml(data.get("keterangan3").toString()));
                    gambar4.setImageResource(R.drawable.inj_botox);
                    keterangan4.setText(Html.fromHtml(data.get("keterangan4").toString()));
                    gambar5.setImageResource(R.drawable.pengompresan_es);
                    keterangan5.setText(Html.fromHtml(data.get("keterangan5").toString()));
                    gambar6.setVisibility(View.GONE);
                    keterangan6.setVisibility(View.GONE);
                    gambar7.setVisibility(View.GONE);
                    keterangan7.setVisibility(View.GONE);
                    gambar8.setVisibility(View.GONE);
                    keterangan8.setVisibility(View.GONE);;
                    gambar9.setVisibility(View.GONE);
                    keterangan9.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        } catch (Exception e){
            return;
        }


        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailMasalahWajah Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "DetailMasalahWajah Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://it.umsida.hanifahdwindasari.beautyask/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
    //scale
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    public class simpleOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            float size = detail_facial.getTextSize();
            Log.d("TextSizeStart", String.valueOf(size));

            //float factor = detector.getScaleFactor();
            float factor = Math.max(0.5f, Math.min(detector.getScaleFactor(), 20f));
            Log.d("Factor", String.valueOf(factor));


            float product = size*factor;
            Log.d("TextSize", String.valueOf(product));

            float safe = Math.abs(product - size);

            if(product <= 100 && product >= 1 && safe < 3){
                //tv.setText("factor= " +factor + "\n" +  "product = \n" + size + " * " + factor + " \n= " + product +"\n" + getString(R.string.hello_world));
                if (product < 18.0f){
                    detail_facial.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan1.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan2.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan3.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan4.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan5.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan6.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan7.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan8.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                    keterangan9.setTextSize(TypedValue.COMPLEX_UNIT_PX,18.0f);
                }else {
                    detail_facial.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan1.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan2.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan3.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan4.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan5.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan6.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan7.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan8.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);
                    keterangan9.setTextSize(TypedValue.COMPLEX_UNIT_PX, product);




                }

            }

            size = detail_facial.getTextSize();
            Log.d("TextSizeEnd", String.valueOf(size));
            return true;
        }
    }
}
