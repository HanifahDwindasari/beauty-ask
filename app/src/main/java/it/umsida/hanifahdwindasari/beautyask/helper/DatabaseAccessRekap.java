package it.umsida.hanifahdwindasari.beautyask.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 01/06/2016.
 */
public class DatabaseAccessRekap {
    private static final String TABLE_NAME = "rekap_data";
    private static final String KEY_NAME = "date";
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccessRekap instance;

    private DatabaseAccessRekap(Context context){
        this.openHelper = new DatabaseOpenHelper(context);
    }
    public static DatabaseAccessRekap getInstance(Context context){
        if (instance == null){
            instance = new DatabaseAccessRekap(context);
        }
        return instance;
    }
    public void open(){
        this.database = openHelper.getWritableDatabase();
    }
    public  void close(){
        if (database != null){
            this.database.close();
        }
    }
    public List<String> getListRekap(){
        List<String> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT date FROM "+TABLE_NAME, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public HashMap getDetailRekap(String date){
        HashMap detail = new HashMap();
        Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME+" WHERE date="+"'"+date+"'", null);
        cursor.moveToFirst();
        detail.put("id",cursor.getString(0));
        detail.put("date",cursor.getString(1));
        detail.put("gejala",cursor.getString(2));
        detail.put("solusi",cursor.getString(3));
        cursor.close();
        return detail;
    }

    public long addRekap(String date, String gejala, String solusi){
        ContentValues contentValues = new ContentValues();
        contentValues.put("date",date);
        contentValues.put("gejala",gejala);
        contentValues.put("solusi",solusi);
        long returnValue = database.insert("rekap_data",null,contentValues);

        return returnValue;
    }

    public boolean deleteRekap(String date){
        return database.delete(TABLE_NAME, KEY_NAME + "='" + date + "'", null) > 0;
    }
}
