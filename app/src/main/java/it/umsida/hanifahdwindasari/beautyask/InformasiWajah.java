package it.umsida.hanifahdwindasari.beautyask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by user on 22/04/2016.
 */
public class InformasiWajah extends AppCompatActivity {
    ImageView menu_informasi1, menu_informasi2, menu_informasi3;
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_menu_informasi);
        super.onCreate(savedInstanceState);
        ImageView menu_informasi1 = (ImageView) findViewById(R.id.informasi1);
        ImageView menu_informasi2 = (ImageView) findViewById(R.id.informasi2);
        ImageView menu_informasi3 = (ImageView) findViewById(R.id.informasi3);
        menu_informasi1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pindah = new Intent(InformasiWajah.this, NavDrawerMasalahWajah.class);
                startActivity(pindah);
            }
        });
        menu_informasi2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pindah2 = new Intent(InformasiWajah.this, NavDrawerFacialWajah.class);
                startActivity(pindah2);
            }
        });
        menu_informasi3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pindah3 = new Intent(InformasiWajah.this, NavDrawerTipsKecantikan.class);
                startActivity(pindah3);
            }
        });
    }
}
